FROM microsoft/dotnet:2.1-runtime
LABEL maintainer="Sophie Tauchert <sophie@999eagle.moe>"
WORKDIR /app
COPY fb5-bot/out/ ./
ENTRYPOINT ["./run.sh"]
