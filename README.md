# FB5-bot

Discord bot for FB5 Game Dev.

## Modules

### Roles

Required permissions (permissions integer 268512320)

- Manage Roles
- View Channels
- Send Messages
- Manage Messages
- Read Message History
- Add Reactions
