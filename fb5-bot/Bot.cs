using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Discord;
using Discord.WebSocket;

using fb5_bot.Modules;
using fb5_bot.Services;

namespace fb5_bot
{
	class Bot
	{
		private ILogger logger;
		private Config config;
		private IServiceProvider services;
		private IServiceScope serviceScope;
		private CancellationTokenSource cancellationTokenSource;
		private IDictionary<string, Type> knownModules;
		private IList<IModule> loadedModules;

		public Bot(IConfiguration config, ILoggerFactory loggerFactory)
		{
			this.config = config.Get<Config>();
			ConfigureServices();

			logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<Bot>();

			void ConfigureServices()
			{
				var services = new ServiceCollection();
				services
					.AddSingleton(config)
					.AddSingleton(this.config)
					.AddSingleton(loggerFactory)
					.AddSingleton<Data.IDataStore, Data.LiteDB.DataStore>()
					.AddScoped((_) => new Discord.WebSocket.DiscordSocketConfig()
					{
						LogLevel = LogSeverity.Debug,
					})
					.AddScoped<DiscordSocketClient>()
					.AddScoped<IPermissionService, fb5_bot.Services.Permissions.PermissionService>()
					.AddScoped<ICommandService, fb5_bot.Services.Commands.CommandService>();

				this.services = services.BuildServiceProvider();
			}
		}

		public Task Run()
		{
			logger.LogInformation("Starting bot");
			serviceScope = services.CreateScope();
			cancellationTokenSource = new CancellationTokenSource();
			var client = serviceScope.ServiceProvider.GetRequiredService<DiscordSocketClient>();
			client.Log += DispatchDiscordLogMessage;

			var commandService = serviceScope.ServiceProvider.GetRequiredService<ICommandService>();
			client.MessageReceived += commandService.DispatchCommand;

			LoadModules();

			client.LoginAsync(TokenType.Bot, config.API.DiscordLoginToken).Wait();
			client.StartAsync().Wait();

			return RunAsync();

			async Task RunAsync()
			{
				try
				{
					await Task.Delay(-1, cancellationTokenSource.Token);
				}
				catch (TaskCanceledException)
				{
					logger.LogInformation("Stopping bot");
				}
				await client.SetStatusAsync(UserStatus.Offline);
				await client.StopAsync();
				await client.LogoutAsync();
				serviceScope.Dispose();
				serviceScope = null;
			}

			void LoadModules()
			{
				knownModules = new Dictionary<string, Type>
				{
					{ "Roles", typeof(fb5_bot.Modules.Roles.Roles) },
				};
				// TODO: Load more modules from config
				loadedModules = new List<IModule>();
				foreach ((string name, var moduleConfig) in config.Modules)
				{
					if (!moduleConfig.Enabled)
					{
						continue;
					}
					if (!knownModules.TryGetValue(name, out var moduleType))
					{
						logger.LogError($"Unknown module {name}");
						continue;
					}
					var module = Activator.CreateInstance(moduleType, serviceScope.ServiceProvider) as IModule;
					if (module == null)
					{
						logger.LogError($"Failed to create instance of module {name}");
						continue;
					}
					if (module.GetName() != name)
					{
						logger.LogWarning($"Module name mismatch. Expected {name}, found {module.GetName()}");
					}
					if (!module.Initialize())
					{
						logger.LogError($"Module {name} failed to initialize");
						continue;
					}
					loadedModules.Add(module);
				}
			}
		}

		public void Stop()
		{
			logger.LogDebug("Sending stop signal");
			cancellationTokenSource.Cancel();
		}

		private async Task DispatchDiscordLogMessage(LogMessage message)
		{
			await Task.Run(() =>
			{
				LogLevel level;
				switch (message.Severity)
				{
					case LogSeverity.Debug:
						level = LogLevel.Trace;
						break;
					case LogSeverity.Verbose:
						level = LogLevel.Debug;
						break;
					case LogSeverity.Info:
						level = LogLevel.Information;
						break;
					case LogSeverity.Warning:
						level = LogLevel.Warning;
						break;
					case LogSeverity.Error:
						level = LogLevel.Error;
						break;
					case LogSeverity.Critical:
						level = LogLevel.Critical;
						break;
					default:
						level = LogLevel.Information;
						break;
				}
				var logger = serviceScope.ServiceProvider.GetRequiredService<ILoggerFactory>().CreateLogger(message.Source);
				logger.Log(level, new EventId(), message.Message, message.Exception, Util.FormatLogMessage);
			});
		}
	}
}
