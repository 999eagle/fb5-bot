using System.Collections.Generic;
using Microsoft.Extensions.Logging;

using fb5_bot.ConfigClasses;

namespace fb5_bot
{
	class Config
	{
		public API API { get; set; }
		public Logging Logging { get; set; }
		public ConfigClasses.Data Data { get; set; }
		public IDictionary<string, Module> Modules { get; set; }
		public Commands Commands { get; set; }
	}

	namespace ConfigClasses
	{
		class API
		{
			public string DiscordLoginToken { get; set; }
		}
		class Logging
		{
			public LogLevel LogLevel { get; set; }
		}
		class Data
		{
			public string ConnectionString { get; set; }
		}
		class Module
		{
			public bool Enabled { get; set; }
		}
		class Commands
		{
			public string Tag { get; set; }
		}
	}
}
