using fb5_bot.Modules;

namespace fb5_bot.Data
{
	public interface IDataStore
	{
		IModuleGuildData<TData> GetGuildData<TData>(IModule module, ulong guildId) where TData : class, new();
		IGuildData GetGuildData(ulong guildId);
	}
}
