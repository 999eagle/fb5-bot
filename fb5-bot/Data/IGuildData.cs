using System;
using System.Collections.Generic;

using fb5_bot.Services;

namespace fb5_bot.Data
{
	public interface IGuildData
	{
		ulong GuildId { get; set; }
		IDictionary<ulong, Permission> RolePermissions { get; set; }

		void Save();
	}
}
