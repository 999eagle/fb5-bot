namespace fb5_bot.Data
{
	public interface IModuleGuildData
	{
		ulong GuildId { get; }
		string ModuleName { get; }
	}

	public interface IModuleGuildData<TData> : IModuleGuildData where TData : class, new()
	{
		TData ModuleData { get; }

		void Save();
	}
}
