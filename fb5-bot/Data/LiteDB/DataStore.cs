using System;
using System.Collections.Generic;
using System.IO;
using LiteDB;
using Microsoft.Extensions.Configuration;

using fb5_bot.Modules;
using fb5_bot.Services;

namespace fb5_bot.Data.LiteDB
{
	class DataStore : IDisposable, IDataStore
	{
		private Config config;
		private LiteDatabase database;
		private LiteCollection<ModuleGuildData> moduleGuildDataCollection;
		private IDictionary<(ulong guildId, string moduleName), IModuleGuildData> moduleGuildDataCache = new Dictionary<(ulong, string), IModuleGuildData>();
		private object moduleGuildDataCacheLock = new object();
		private LiteCollection<GuildData> guildDataCollection;

		public DataStore(Config config)
		{
			this.config = config;
			// TODO: Parse filename instead of assuming that the connection string is just the filename
			var directory = Path.GetDirectoryName(config.Data.ConnectionString);
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			database = new LiteDatabase(config.Data.ConnectionString);
			moduleGuildDataCollection = database.GetCollection<ModuleGuildData>("module_guild_data");
			moduleGuildDataCollection.EnsureIndex(d => d.ID);
			moduleGuildDataCollection.EnsureIndex(d => d.GuildId);
			moduleGuildDataCollection.EnsureIndex(d => d.ModuleName);

			guildDataCollection = database.GetCollection<GuildData>("guild_data");
			guildDataCollection.EnsureIndex(d => d.GuildId);
		}

		IModuleGuildData<TData> IDataStore.GetGuildData<TData>(IModule module, ulong guildId)
		{
			var moduleName = module.GetName();
			lock (moduleGuildDataCacheLock)
			{
				if (moduleGuildDataCache.TryGetValue((guildId, moduleName), out var data)) return data as IModuleGuildData<TData>;

				var dbData = moduleGuildDataCollection.FindOne(d => d.GuildId == guildId && d.ModuleName == moduleName);
				if (dbData == null)
				{
					dbData = new ModuleGuildData
					{
						ID = ObjectId.NewObjectId(),
						GuildId = guildId,
						ModuleName = moduleName,
						ModuleData = null,
					};
					moduleGuildDataCollection.Insert(dbData);
				}
				var result = new ModuleGuildData<TData>(dbData, database.Mapper, this);
				moduleGuildDataCache.Add((guildId, moduleName), result);
				return result;
			}
		}

		IGuildData IDataStore.GetGuildData(ulong guildId)
		{
			var dbData = guildDataCollection.FindOne(d => d.GuildId == guildId);
			if (dbData == null)
			{
				dbData = new GuildData
				{
					GuildId = guildId,
					RolePermissions = new Dictionary<ulong, Permission>(),
				};
				guildDataCollection.Insert(dbData);
			}
			dbData.dataStore = this;
			return dbData;
		}

		internal void SaveGuildData(ModuleGuildData dbData)
		{
			moduleGuildDataCollection.Update(dbData);
		}

		internal void SaveGuildData(GuildData dbData)
		{
			guildDataCollection.Update(dbData);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				database?.Dispose();
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
