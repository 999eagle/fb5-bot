using System;
using System.Collections.Generic;
using LiteDB;

using fb5_bot.Services;

namespace fb5_bot.Data.LiteDB
{
	class GuildData : IGuildData
	{
		[BsonId]
		public ulong GuildId { get; set; }

		public IDictionary<ulong, Permission> RolePermissions { get; set; }

		[BsonIgnore]
		internal DataStore dataStore;
		public void Save()
		{
			dataStore.SaveGuildData(this);
		}
	}
}
