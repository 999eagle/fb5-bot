using LiteDB;

namespace fb5_bot.Data.LiteDB
{
	class ModuleGuildData
	{
		[BsonId]
		public ObjectId ID { get; set; }
		public ulong GuildId { get; set; }
		public string ModuleName { get; set; }
		public BsonDocument ModuleData { get; set; }
	}

	class ModuleGuildData<TData> : IModuleGuildData<TData> where TData : class, new()
	{
		public ulong GuildId { get { return dbModel.GuildId; } }
		public string ModuleName { get { return dbModel.ModuleName; } }
		public TData ModuleData { get; private set; }

		private ModuleGuildData dbModel;
		private BsonMapper mapper;
		private DataStore store;

		public ModuleGuildData(ModuleGuildData dbModel, BsonMapper mapper, DataStore store)
		{
			this.mapper = mapper;
			this.dbModel = dbModel;
			this.store = store;
			if (dbModel.ModuleData == null)
			{
				ModuleData = new TData();
			}
			else
			{
				ModuleData = mapper.ToObject<TData>(dbModel.ModuleData);
			}
		}

		public void Save()
		{
			dbModel.ModuleData = mapper.ToDocument(ModuleData);
			store.SaveGuildData(dbModel);
		}
	}
}
