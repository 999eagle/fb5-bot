using System;

namespace fb5_bot.Modules
{
	public interface IModule
	{
		string GetName();
		bool Initialize();
	}
}
