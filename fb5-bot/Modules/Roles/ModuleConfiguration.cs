using System;

namespace fb5_bot.Modules.Roles
{
	class ModuleConfiguration
	{
		public bool Enabled { get; set; }
		public string Test { get; set; }
	}
}
