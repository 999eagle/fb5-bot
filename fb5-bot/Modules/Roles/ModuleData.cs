using System.Collections.Generic;
using Discord.WebSocket;

namespace fb5_bot.Modules.Roles
{
	class ModuleData
	{
		public class ReactionMessage
		{
			public ulong MessageId { get; set; }
			public IList<string> ValidEmotes { get; set; }
		}
		public IList<ReactionMessage> ReactionMessages { get; set; }
		public IDictionary<string, ulong> EmoteRoleMapping { get; set; }

		public ModuleData()
		{
			ReactionMessages = new List<ReactionMessage>();
			EmoteRoleMapping = new Dictionary<string, ulong>();
		}
	}
}
