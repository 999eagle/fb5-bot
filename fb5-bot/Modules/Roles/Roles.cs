using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using fb5_bot.Data;
using fb5_bot.Services;
using fb5_bot.Services.Commands;

namespace fb5_bot.Modules.Roles
{
	class Roles : IModule
	{
		private IConfiguration globalConfig;
		private ModuleConfiguration config;
		private DiscordSocketClient client;
		private ILogger logger;
		private IDataStore dataStore;
		private ICommandService commandService;

		public Roles(IServiceProvider services)
		{
			globalConfig = services.GetRequiredService<IConfiguration>();
			client = services.GetRequiredService<DiscordSocketClient>();
			logger = services.GetRequiredService<ILoggerFactory>().CreateLogger<Roles>();
			dataStore = services.GetRequiredService<IDataStore>();
			commandService = services.GetRequiredService<ICommandService>();
		}

		public string GetName() => "Roles";

		public bool Initialize()
		{
			logger.LogDebug("Initializing");
			// TODO: Check for bot user permissions
			config = globalConfig.GetValue<ModuleConfiguration>("Modules:Roles");
			client.ReactionAdded += OnReactionAdded;
			client.ReactionRemoved += OnReactionRemoved;
			client.MessageDeleted += OnMessageDeleted;
			commandService.RegisterAllCommandsInClass(this);
			logger.LogDebug("Initialized");
			return true;
		}

		[Command("roles", Subcommands = new[] { "mapping", "list" }, Scope = CommandScope.Guild,
			HelpText = "List emote mappings")]
		[Permission(Permission.Mod)]
		private async Task CommandListEmoteMapping(SocketMessage message)
		{
			if (!(message.Channel is SocketGuildChannel guildChannel)) return;
			var data = dataStore.GetGuildData<ModuleData>(this, guildChannel.Guild.Id);
			if (data.ModuleData.EmoteRoleMapping.Any())
			{
				await message.Channel.SendMessageAsync(String.Join("\n",
					data.ModuleData.EmoteRoleMapping.Select(map =>
					{
						return ReadEmoteKey(map.Key) + ": " + MentionUtils.MentionRole(map.Value);
					})));
			}
			else
			{
				await message.Channel.SendMessageAsync("No emote mapping defined.");
			}
		}

		[Command("roles", Subcommands = new[] { "mapping", "add" }, Scope = CommandScope.Guild,
			HelpText = "Add emote mapping")]
		[Permission(Permission.Admin)]
		private async Task CommandAddEmoteMapping(SocketMessage message, IEmote emote, SocketRole role)
		{
			if (!(message.Channel is SocketGuildChannel guildChannel)) return;
			var data = dataStore.GetGuildData<ModuleData>(this, guildChannel.Guild.Id);
			var emoteKey = GetEmoteKey(emote);
			if (data.ModuleData.EmoteRoleMapping.ContainsKey(emoteKey))
			{
				await message.Channel.SendMessageAsync("Mapping for this emote already exists.");
				return;
			}
			data.ModuleData.EmoteRoleMapping.Add(emoteKey, role.Id);
			data.Save();
			await message.Channel.SendMessageAsync("Mapping added");
		}

		[Command("roles", Subcommands = new[] { "mapping", "remove" }, Scope = CommandScope.Guild,
			HelpText = "Remove emote mapping")]
		[Permission(Permission.Admin)]
		private async Task CommandRemoveEmoteMapping(SocketMessage message, IEmote emote)
		{
			if (!(message.Channel is SocketGuildChannel guildChannel)) return;
			if (!await Util.CanUseEmote(emote, client))
			{
				await message.Channel.SendMessageAsync("I can't use that emote");
				return;
			}
			var data = dataStore.GetGuildData<ModuleData>(this, guildChannel.Guild.Id);
			var emoteKey = GetEmoteKey(emote);
			if (!data.ModuleData.EmoteRoleMapping.ContainsKey(emoteKey))
			{
				await message.Channel.SendMessageAsync("Mapping for this emote doesn't exist.");
				return;
			}
			data.ModuleData.EmoteRoleMapping.Remove(emoteKey);
			data.Save();
			await message.Channel.SendMessageAsync("Mapping removed");
		}

		[Command("roles", Subcommands = new[] { "message", "set" }, Scope = CommandScope.Guild,
			HelpText = "Set last message as role reaction message")]
		[Permission(Permission.Admin)]
		private async Task CommandSetRoleReactionMessage(SocketMessage message, IEmote[] emotes)
		{
			if (!(message.Channel is SocketGuildChannel guildChannel)) return;
			var data = dataStore.GetGuildData<ModuleData>(this, guildChannel.Guild.Id);
			var previousMessage = (await message.Channel.GetMessagesAsync(message, Direction.Before, limit: 5)
				.Select(ms => ms.FirstOrDefault(m => m.Author == message.Author))
				.Where(m => m != null)
				.FirstOrDefault()) as IUserMessage;
			if (previousMessage == null)
			{
				await message.Channel.SendMessageAsync("Please send a message first");
				return;
			}
			var reactionMessage = data.ModuleData.ReactionMessages.FirstOrDefault(m => m.MessageId == previousMessage.Id);
			if (reactionMessage == null)
			{
				reactionMessage = new ModuleData.ReactionMessage
				{
					MessageId = previousMessage.Id,
					ValidEmotes = emotes.Select(GetEmoteKey).ToList(),
				};
				data.ModuleData.ReactionMessages.Add(reactionMessage);
			}
			else
			{
				reactionMessage.ValidEmotes = emotes.Select(GetEmoteKey).ToList();
			}
			data.Save();
			await previousMessage.RemoveAllReactionsAsync();
			foreach (var emote in emotes)
			{
				await previousMessage.AddReactionAsync(emote);
			}
			await message.DeleteAsync();
		}

		private string GetEmoteKey(IEmote iEmote)
		{
			switch (iEmote)
			{
				case Emote emote:
					return $"{emote.Name}:{emote.Id}";
				case Emoji emoji:
					return $"{emoji.Name}";
				default:
					throw new ArgumentException();
			}
		}

		private IEmote ReadEmoteKey(string key)
		{
			if (key.Contains(':'))
				return Emote.Parse($"<:{key}>");
			else
				return new Emoji(key);
		}

		private Task OnMessageDeleted(Cacheable<IMessage, ulong> message, ISocketMessageChannel channel)
		{
			return Task.Run(() =>
			{
				if (!(channel is IGuildChannel guildChannel)) return;
				var data = dataStore.GetGuildData<ModuleData>(this, guildChannel.GuildId);
				var reactionMessage = data.ModuleData.ReactionMessages.FirstOrDefault(m => m.MessageId == message.Id);
				if (reactionMessage == null) return;
				data.ModuleData.ReactionMessages.Remove(reactionMessage);
				data.Save();
			});
		}

		private async Task OnReactionAdded(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
		{
			if (!(channel is IGuildChannel guildChannel)) return;
			if (!(reaction.User.Value is SocketGuildUser user)) return;
			var data = dataStore.GetGuildData<ModuleData>(this, guildChannel.GuildId);
			if (reaction.UserId == client.CurrentUser.Id) return;

			var emotes = data.ModuleData.ReactionMessages.FirstOrDefault(m => m.MessageId == message.Id)?.ValidEmotes;
			if (emotes == null) return;
			var emoteKey = GetEmoteKey(reaction.Emote);
			if (!emotes.Contains(emoteKey))
			{
				logger.LogDebug($"Guild {guildChannel.Guild.Name}({guildChannel.GuildId}), Channel {channel.Name}({channel.Id}): removing reaction {reaction.Emote.Name} by {user} (Invalid emote key)");
				await (await message.GetOrDownloadAsync()).RemoveReactionAsync(reaction.Emote, user);
				return;
			}
			if (!data.ModuleData.EmoteRoleMapping.TryGetValue(emoteKey, out var roleId))
			{
				logger.LogError($"Guild {guildChannel.Guild.Name}({guildChannel.GuildId}): Emote key {emoteKey} not mapped to any role");
				await (await message.GetOrDownloadAsync()).RemoveReactionAsync(reaction.Emote, user);
				return;
			}
			var role = guildChannel.Guild.GetRole(roleId);
			if (role == null)
			{
				logger.LogError($"Guild {guildChannel.Guild.Name}({guildChannel.GuildId}): Role {roleId} mapped to emote {emoteKey} not found");
				await (await message.GetOrDownloadAsync()).RemoveReactionAsync(reaction.Emote, user);
				return;
			}
			await user.AddRoleAsync(role);
		}

		private async Task OnReactionRemoved(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
		{
			if (!(channel is IGuildChannel guildChannel)) return;
			if (!(reaction.User.Value is SocketGuildUser user)) return;
			var data = dataStore.GetGuildData<ModuleData>(this, guildChannel.GuildId);

			var reactionMessage = data.ModuleData.ReactionMessages.FirstOrDefault(m => m.MessageId == message.Id);
			if (reactionMessage == null) return;
			var emoteKey = GetEmoteKey(reaction.Emote);
			if (!data.ModuleData.EmoteRoleMapping.TryGetValue(emoteKey, out var roleId))
			{
				logger.LogError($"Guild {guildChannel.Guild.Name}({guildChannel.GuildId}): Emote key {emoteKey} not mapped to any role");
				return;
			}
			var role = guildChannel.Guild.GetRole(roleId);
			if (role == null)
			{
				logger.LogError($"Guild {guildChannel.Guild.Name}({guildChannel.GuildId}): Role {roleId} mapped to emote {emoteKey} not found");
				return;
			}
			await user.RemoveRoleAsync(role);
		}
	}
}
