﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace fb5_bot
{
	class Program
	{
		static ILogger logger;
		static Task runTask;
		static Bot botInstance;

		static void Main(string[] args)
		{
			AsyncMain().Wait();

			async Task AsyncMain()
			{
				var waitEvent = new ManualResetEventSlim(false);
				using (var tokenSource = new CancellationTokenSource())
				{
					AttachShutdownEvents(tokenSource, waitEvent);

					try
					{
						Start();

						var waitForCancel = new TaskCompletionSource<object>(TaskCreationOptions.RunContinuationsAsynchronously);
						tokenSource.Token.Register((state) =>
						{
							((TaskCompletionSource<object>)state).TrySetResult(null);
						}, waitForCancel);
						await waitForCancel.Task;

						Stop();
					}
					finally
					{
						waitEvent.Set();
					}
				}
			}
		}

		static void AttachShutdownEvents(CancellationTokenSource tokenSource, ManualResetEventSlim waitEvent)
		{
			void Shutdown()
			{
				if (!tokenSource.IsCancellationRequested)
				{
					try
					{
						tokenSource.Cancel();
					}
					catch (ObjectDisposedException) { }
				}
				waitEvent.Wait();
			}

			AppDomain.CurrentDomain.ProcessExit += (sender, eventArgs) => Shutdown();
			Console.CancelKeyPress += (sender, eventArgs) =>
			{
				Shutdown();
				eventArgs.Cancel = true;
			};
		}

		static void Start()
		{
			var configBuilder = new ConfigurationBuilder();
			configBuilder.AddJsonFile("config.json");
			var config = configBuilder.Build();
			var loggerFactory = new LoggerFactory();
			loggerFactory.AddConsole();
			logger = loggerFactory.CreateLogger("Init");
			logger.LogInformation("fb5-bot starting");

			try
			{
				botInstance = new Bot(config, loggerFactory);
				runTask = botInstance.Run();
			}
			catch (Exception ex)
			{
				logger.LogCritical(ex, "An unhandled exception was thrown during startup");
				throw;
			}
		}

		static void Stop()
		{
			logger.LogDebug("fb5-bot stopping");
			botInstance?.Stop();
			if (runTask != null)
			{
				try
				{
					runTask.GetAwaiter().GetResult();
				}
				catch (Exception ex)
				{
					logger?.LogError(ex, "An unhandled exception was thrown during shutdown");
				}
			}
			logger?.LogInformation("fb5-bot stopped");
		}
	}
}
