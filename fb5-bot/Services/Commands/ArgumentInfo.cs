using System;
using System.Reflection;

namespace fb5_bot.Services.Commands
{
	class ParameterArgumentInfo : IArgumentInfo
	{
		public Type Type { get; }
		public bool IsOptional { get; }
		public string Name { get; }

		public ParameterArgumentInfo(ParameterInfo info)
		{
			Type = info.ParameterType;
			IsOptional = info.GetCustomAttribute<OptionalAttribute>() != null;
			Name = info.Name;
		}
	}

	class ArrayElementArgumentInfo : IArgumentInfo
	{
		public Type Type { get; }
		public bool IsOptional { get => false; }
		public string Name { get; }

		public ArrayElementArgumentInfo(IArgumentInfo arrayArgument)
		{
			if (!arrayArgument.Type.IsArray) throw new ArgumentException("Type of argument must be an array type", nameof(arrayArgument));
			Type = arrayArgument.Type.GetElementType();
			Name = arrayArgument.Name;
		}
	}
}
