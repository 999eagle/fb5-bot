using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Discord;
using Discord.WebSocket;

namespace fb5_bot.Services.Commands
{
	class ArgumentParser
	{
		private ILogger logger;
		private IDictionary<Type, Func<string, ParseContext, (bool success, object result)>> simpleParsers;

		public ArgumentParser(ILoggerFactory loggerFactory)
		{
			this.logger = loggerFactory.CreateLogger<ArgumentParser>();
			simpleParsers = new Dictionary<Type, Func<string, ParseContext, (bool, object)>>
			{
				{ typeof(string), ParseString },
				{ typeof(int), ParseInt },
				{ typeof(SocketUser), ParseSocketUser },
				{ typeof(SocketRole), ParseSocketRole },
				{ typeof(Emote), ParseEmote },
				{ typeof(Emoji), ParseEmoji },
				{ typeof(IEmote), ParseIEmote },
			};
		}

		(bool, object) ParseString(string input, ParseContext context) => (input != null, input);
		(bool, object) ParseInt(string input, ParseContext context)
		{
			if (input != null && Int32.TryParse(input, out var val)) return (true, val);
			return (false, null);
		}
		(bool, object) ParseSocketUser(string input, ParseContext context)
		{
			if (input != null && (MentionUtils.TryParseUser(input, out var userId) || UInt64.TryParse(input, out userId)))
			{
				var user = (SocketUser)context.Guild?.GetUser(userId) ?? context.GroupChannel?.GetUser(userId);
				if (user != null) return (true, user);
			}
			return (false, null);
		}
		(bool, object) ParseSocketRole(string input, ParseContext context)
		{
			if (input != null && (MentionUtils.TryParseRole(input, out var roleId) || UInt64.TryParse(input, out roleId)))
			{
				var role = context.Guild?.GetRole(roleId);
				if (role != null) return (true, role);
			}
			return (false, null);
		}
		(bool, object) ParseEmote(string input, ParseContext context)
		{
			if (input != null && Emote.TryParse(input, out var emote)) return (true, emote);
			return (false, null);
		}
		(bool, object) ParseEmoji(string input, ParseContext context)
		{
			if (input == null || !EmojiData.Emoji.TryGetSingleEmoji(input, out var emoji)) return (false, null);
			return (true, new Emoji(input));
		}
		(bool, object) ParseIEmote(string input, ParseContext context)
		{
			var result = ParseEmote(input, context);
			if (result.Item1) return result;
			return ParseEmoji(input, context);
		}

		public string GenerateUsageString(IEnumerable<IArgumentInfo> arguments)
		{
			string usageString = "";
			foreach (var arg in arguments)
			{
				string usage = GetSingleArgumentUsageString(arg) ?? $"{arg.Name}";
				if (arg.IsOptional) usage = $"[{usage}]";
				usageString += " " + usage;
			}
			return usageString.TrimStart();

			string GetSingleArgumentUsageString(IArgumentInfo arg)
			{
				switch (arg)
				{
					case IArgumentInfo user when (user.Type == typeof(SocketUser)):
						return "<@user>";
					case IArgumentInfo role when (role.Type == typeof(SocketRole)):
						return "<@role>";
					case IArgumentInfo val when (val.Type == typeof(string) || val.Type == typeof(int)):
						return $"<{val.Name}>";
					case IArgumentInfo val when (val.Type == typeof(IEmote) || val.Type == typeof(Emote) || val.Type == typeof(Emoji)):
						return $"<emoji>";
					case IArgumentInfo a when a.Type.IsArray:
						var elementUsage = GetSingleArgumentUsageString(new ArrayElementArgumentInfo(arg));
						if (elementUsage == null) return null;
						return $"({elementUsage} ...)";
					default:
						logger.LogWarning($"Unknown argument type {arg.Type.FullName} for argument {arg.Name}");
						return null;
				}
			}
		}

		public bool TryParseArguments(ParseContext context, IEnumerable<IArgumentInfo> arguments, IEnumerable<string> inputs, out IEnumerable<object> parsed)
		{
			parsed = null;
			var arg = arguments.FirstOrDefault();
			var input = inputs.FirstOrDefault();
			if (arg == null)
			{
				if (input == null)
				{
					// end of input list
					parsed = new object[0];
					return true;
				}
				else
				{
					// more input than arguments
					return false;
				}
			}
			object parsedObject = null;
			int parsedInputCount = 0;
			switch (arg)
			{
				case IArgumentInfo a when (simpleParsers.ContainsKey(a.Type)):
					(bool success, object result) = simpleParsers[a.Type](input, context);
					if (success)
					{
						parsedInputCount = 1;
						parsedObject = result;
					}
					break;
				case IArgumentInfo a when (a.Type.IsArray && a.Type.GetElementType() is Type elementType && simpleParsers.ContainsKey(elementType)):
					var objects = new List<object>();
					var items = inputs;
					while (items.Any())
					{
						var tuple = simpleParsers[elementType](items.First(), context);
						items = items.Skip(1);
						if (tuple.success)
						{
							objects.Add(tuple.result);
						}
						else
						{
							break;
						}
					}
					if (objects.Any())
					{
						parsedInputCount = objects.Count;
						var array = Array.CreateInstance(elementType, objects.Count);
						for (int i = 0; i < array.Length; i++)
						{
							array.SetValue(objects[i], i);
						}
						parsedObject = array;
					}
					break;
				default:
					logger.LogWarning($"Unknown argument type {arg.Type.FullName} for argument {arg.Name} of command {context.Command}");
					break;
			}
			if (parsedInputCount == 0 && !arg.IsOptional)
			{
				// parsing failed, but argument isn't optional
				return false;
			}
			if (TryParseArguments(context, arguments.Skip(1), inputs.Skip(parsedInputCount), out var subParsed))
			{
				parsed = new object[] { parsedObject }.Concat(subParsed);
				return true;
			}
			else if (arg.IsOptional && TryParseArguments(context, arguments.Skip(1), inputs, out subParsed))
			{
				parsed = new object[] { null }.Concat(subParsed);
				return true;
			}
			return false;
		}
	}
}
