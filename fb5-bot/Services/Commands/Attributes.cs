using System;

namespace fb5_bot.Services.Commands
{
	[AttributeUsage(AttributeTargets.Method)]
	public class CommandAttribute : Attribute, ICommandDescription
	{
		public string[] Commands { get; }
		public string Subcommand { set { Subcommands = new[] { value }; } }
		public string[] Subcommands { get; set; }
		public string HelpText { get; set; }
		public CommandScope Scope { get; set; } = CommandScope.All;

		public CommandAttribute(params string[] commands)
		{
			Commands = commands;
		}
	}

	[AttributeUsage(AttributeTargets.Parameter)]
	public class OptionalAttribute : Attribute { }
}
