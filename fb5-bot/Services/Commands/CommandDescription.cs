using System;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace fb5_bot.Services.Commands
{
	class CommandDescription : ICommandDescription
	{
		public string[] Commands { get; }
		public string[] Subcommands { get; set; }
		public string HelpText { get; set; }
		public CommandScope Scope { get; set; } = CommandScope.All;

		public string UsageString { get; set; }
		public Func<SocketMessage, string[], Task<bool>> Invoke { get; set; }
		public IPermissionInfo PermissionInfo { get; set; }

		public CommandDescription(params string[] commands)
		{
			Commands = commands;
		}

		public static CommandDescription CreateCommandDescription(ICommandDescription original)
		{
			if (original is CommandDescription descr) return descr;

			return new CommandDescription(original.Commands)
			{
				HelpText = original.HelpText,
				Subcommands = original.Subcommands,
				Scope = original.Scope,
			};
		}

		public string GetFullUsage() => $"{Commands[0]}{(Subcommands == null ? "" : $" {String.Join(" ", Subcommands)}")} {UsageString}";
	}
}
