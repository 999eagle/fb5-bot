using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Discord;
using Discord.WebSocket;

namespace fb5_bot.Services.Commands
{
	class CommandService : ICommandService
	{
		private ILogger logger;
		private Config config;
		private ArgumentParser parser;
		private IList<CommandDescription> registeredCommands;
		private IPermissionService permissionService;

		public CommandService(ILoggerFactory loggerFactory, Config config, IPermissionService permissionService)
		{
			this.logger = loggerFactory.CreateLogger<CommandService>();
			this.config = config;
			this.permissionService = permissionService;
			parser = new ArgumentParser(loggerFactory);
			registeredCommands = new List<CommandDescription>();

			RegisterAllCommandsInClass(this);
		}

		[Command("help", HelpText = "Shows this help")]
		private async Task HelpCommand(SocketMessage message)
		{
			var text = "**fb5-bot**\nMade by **The999eagle#1337**\n";
			foreach (var command in registeredCommands)
			{
				if (command.HelpText == null) continue;
				text += $"\n`{config.Commands.Tag}{command.GetFullUsage()}`";
				if (command.HelpText != "") text += $": {command.HelpText}";
			}
			await (await message.Author.GetOrCreateDMChannelAsync()).SendMessageAsync(text);
		}

		public Task DispatchCommand(SocketMessage message)
		{
			var backgroundParsing = Task.Run(() =>
			{
				if (message.Content.Length == 0) return;

				var command = message.Content.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
				if (!config.Commands.Tag.EndsWith(" ") && command[0].StartsWith(config.Commands.Tag))
				{
					// move command tag into its own list element
					if (command[0].Length == config.Commands.Tag.Length)
					{
						command.RemoveAt(0);
					}
					else
					{
						command[0] = command[0].Substring(config.Commands.Tag.Length);
					}
					command.Insert(0, config.Commands.Tag);
				}
				var tag = config.Commands.Tag.TrimEnd();
				var split = command.ToArray();
				if (split[0] == tag)
				{
					var success = false;
					foreach (var cmd in registeredCommands)
					{
						success = cmd.Invoke(message, split).GetAwaiter().GetResult();
						if (success) break;
					}
					if (!success)
					{
						message.Channel.SendMessageAsync($"Use {config.Commands.Tag}help to get a list of available commands").GetAwaiter().GetResult();
					}
				}
			});
			backgroundParsing.ContinueWith((task) =>
			{
				if (task.Exception != null)
				{
					logger.LogError(task.Exception, "Unhandled exception thrown while dispatching command");
				}
			});
			return Task.CompletedTask;
		}

		public void RegisterAllCommandsInClass<T>(T instance) where T : class
		{
			if (instance == null) throw new ArgumentNullException(nameof(instance));
			var classType = typeof(T).GetTypeInfo();
			foreach (var method in classType.DeclaredMethods.Where(m => m.GetCustomAttribute<CommandAttribute>() != null))
			{
				var attr = method.GetCustomAttribute<CommandAttribute>();
				RegisterCommand(instance, method, attr);
			}
			logger.LogInformation($"Registered all commands in type {classType.FullName}");
		}

		public void RegisterCommand(object instance, MethodInfo method, ICommandDescription description)
		{
			var command = CommandDescription.CreateCommandDescription(description);
			var arguments = method.GetParameters().Skip(1).Select<ParameterInfo, IArgumentInfo>(p => new ParameterArgumentInfo(p));

			command.UsageString = parser.GenerateUsageString(arguments);
			command.Invoke = CommandInvoke;
			command.PermissionInfo = permissionService.GetCommandPermissionInfo(method);
			registeredCommands.Add(command);

			logger.LogInformation($"Registered command {command.Commands[0]}");

			async Task<bool> CommandInvoke(SocketMessage message, string[] split)
			{
				var parseIdx = 1;
				if (split.Length <= parseIdx || !description.Commands.Contains(split[parseIdx++])) return false;
				if (description.Subcommands != null)
				{
					foreach (var sub in description.Subcommands)
					{
						if (split.Length <= parseIdx || split[parseIdx++] != sub) return false;
					}
				}
				if (!description.Scope.HasFlag(CommandScope.DM) && message.Channel is SocketDMChannel)
				{
					await message.Channel.SendMessageAsync("This command may not be used in a private chat");
					return true;
				}
				if (!description.Scope.HasFlag(CommandScope.Group) && message.Channel is SocketGroupChannel)
				{
					await message.Channel.SendMessageAsync("This command may not be used in a group chat");
					return true;
				}
				if (!description.Scope.HasFlag(CommandScope.Guild) && message.Channel is SocketGuildChannel)
				{
					await message.Channel.SendMessageAsync("This command may not be used in a guild chat");
					return true;
				}

				var context = new ParseContext(message, split[1]);
				if (parser.TryParseArguments(context, arguments, split.Skip(parseIdx), out var parsed))
				{
					if (message.Author is IGuildUser guildUser && !permissionService.UserHasPermission(guildUser, command.PermissionInfo))
					{
						await message.Channel.SendMessageAsync("You don't have permission to use this command.");
						return true;
					}
					var task = method.Invoke(instance, new object[] { message }.Concat(parsed).ToArray());
					if (task is Task<bool> boolTask)
					{
						if (await boolTask) return true;
					}
					else if (task is Task t)
					{
						await t;
						return true;
					}
				}

				var commandsWithSameName = registeredCommands.Where(c => c.Commands.Contains(split[1]));
				if (commandsWithSameName.Count() > 1 && split.Length >= 3)
				{
					// subcommands to the current command exist and we have parameters
					var sub = commandsWithSameName.FirstOrDefault(c => c.Subcommands != null && c.Subcommands.Zip(split.Skip(2), (a, b) => a == b).All(b => b));
					if (sub != null && sub != command)
						// first argument is actually a subcommand, but we're not that subcommand
						return false;
					if (sub == command)
						commandsWithSameName = new[] { command };
				}
				commandsWithSameName = commandsWithSameName.Where(c => c.HelpText != null);
				if (!commandsWithSameName.Any())
				{
					return false;
				}
				var text = "Usage:";
				foreach (var c in commandsWithSameName)
				{
					text += $"\n`{config.Commands.Tag}{c.GetFullUsage()}`";
				}
				await message.Channel.SendMessageAsync(text);
				return true;
			}
		}
	}
}
