using Discord.WebSocket;

namespace fb5_bot.Services.Commands
{
	class ParseContext
	{
		public SocketMessage OriginalMessage { get; }
		public SocketGuild Guild { get; }
		public SocketGuildChannel GuildChannel { get; }
		public SocketGuildUser GuildAuthor { get; }
		public SocketGroupChannel GroupChannel { get; }
		public string Command { get; }

		public ParseContext(SocketMessage message, string command)
		{
			OriginalMessage = message;
			GuildChannel = message.Channel as SocketGuildChannel;
			GuildAuthor = message.Author as SocketGuildUser;
			Guild = GuildChannel.Guild;
			GroupChannel = message.Channel as SocketGroupChannel;
			Command = command;
		}
	}
}
