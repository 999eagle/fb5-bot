using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace fb5_bot.Services
{
	public interface IArgumentInfo
	{
		Type Type { get; }
		bool IsOptional { get; }
		string Name { get; }
	}

	public interface ICommandDescription
	{
		string[] Commands { get; }
		string[] Subcommands { get; }
		string HelpText { get; }
		CommandScope Scope { get; }
	}

	public interface ICommandService
	{
		Task DispatchCommand(SocketMessage message);
		void RegisterAllCommandsInClass<T>(T instance) where T : class;
		void RegisterCommand(object instance, MethodInfo method, ICommandDescription description);
	}

	[Flags]
	public enum CommandScope
	{
		None = 0x0,

		DM = 0x01,
		Group = 0x02,
		Guild = 0x04,

		NonDM = Group | Guild,
		NonGroup = DM | Guild,
		NonGuild = DM | Group,

		All = DM | Group | Guild,
	}
}
