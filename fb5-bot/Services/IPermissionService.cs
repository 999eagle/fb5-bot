using System;
using System.Reflection;

using Discord;

namespace fb5_bot.Services
{
	public interface IPermissionService
	{
		bool UserHasPermission(IGuildUser user, IPermissionInfo permissionInfo);
		IPermissionInfo GetCommandPermissionInfo(MethodInfo info);
	}

	public interface IPermissionInfo
	{
		Permission MinimumPermission { get; }
	}

	public enum Permission
	{
		Everyone = 0,
		Mod = 1,
		Admin = 2,
		Owner = 3,
	}

	[AttributeUsage(AttributeTargets.Method)]
	public class PermissionAttribute : Attribute, IPermissionInfo
	{
		public Permission MinimumPermission { get; }

		public PermissionAttribute(Permission minimumPermission)
		{
			MinimumPermission = minimumPermission;
		}
	}
}
