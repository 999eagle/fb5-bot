using System;

namespace fb5_bot.Services.Permissions
{
	class PermissionInfo : IPermissionInfo
	{
		public Permission MinimumPermission { get; internal set; }
	}
}
