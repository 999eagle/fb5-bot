using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using Discord;

using fb5_bot.Data;

namespace fb5_bot.Services.Permissions
{
	class PermissionService : IPermissionService
	{
		private ILogger logger;
		private IDataStore dataStore;
		private Regex adminRoleRegex;
		private Regex modRoleRegex;

		public PermissionService(ILoggerFactory loggerFactory, IDataStore dataStore)
		{
			logger = loggerFactory.CreateLogger<PermissionService>();
			this.dataStore = dataStore;
			adminRoleRegex = new Regex("^admin(istrator)?$");
			modRoleRegex = new Regex("^mod(erator)?$");
		}

		bool IPermissionService.UserHasPermission(IGuildUser user, IPermissionInfo permissionInfo)
		{
			var data = dataStore.GetGuildData(user.GuildId);
			if (permissionInfo.MinimumPermission == Permission.Everyone) return true;
			if (user.Guild.OwnerId == user.Id && permissionInfo.MinimumPermission <= Permission.Owner) return true;
			if (data.RolePermissions.Count == 0)
			{
				return user.RoleIds
					.Select(user.Guild.GetRole)
					.Select(r => (isAdmin: adminRoleRegex.IsMatch(r.Name) || r.Permissions.Administrator, isMod: modRoleRegex.IsMatch(r.Name)))
					.Any(t => (t.isMod && permissionInfo.MinimumPermission <= Permission.Mod) || (t.isAdmin && permissionInfo.MinimumPermission <= Permission.Admin));
			}
			else
			{
				foreach ((ulong roleId, Permission rolePermission) in data.RolePermissions)
				{
					if (user.RoleIds.Contains(roleId) && rolePermission >= permissionInfo.MinimumPermission) return true;
				}
			}
			return false;
		}

		IPermissionInfo IPermissionService.GetCommandPermissionInfo(MethodInfo info)
		{
			if (info.GetCustomAttribute<PermissionAttribute>() is PermissionAttribute attribute) return attribute;
			return new PermissionInfo { MinimumPermission = Permission.Everyone };
		}
	}
}
