using System;
using System.Threading.Tasks;
using Discord;

namespace fb5_bot
{
	static class Util
	{
		public static string FormatLogMessage(string message, Exception exception)
		{
			var m = "";
			if (!String.IsNullOrWhiteSpace(message))
			{
				m = message;
			}
			if (exception != null)
			{
				if (!String.IsNullOrWhiteSpace(m))
				{
					m += "\n";
				}
				m += $"Exception {exception.GetType().Name} thrown in {exception.Source}:\n{exception.Message}";
			}
			return m;
		}

		public static async Task<bool> CanUseEmote(Emote emote, IDiscordClient client)
		{
			var guilds = await client.GetGuildsAsync();
			foreach (var guild in guilds)
			{
				if (await guild.GetEmoteAsync(emote.Id) != null)
					return true;
			}
			return false;
		}

		public static async Task<bool> CanUseEmote(IEmote emote, IDiscordClient client)
		{
			if (emote is Emote e) return await CanUseEmote(e, client);
			if (emote is Emoji emoji) return true;
			throw new ArgumentException();
		}
	}
}
