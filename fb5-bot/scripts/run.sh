#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")"
SCRIPT_DIR="$(pwd)"
BINARY_FILE="${SCRIPT_DIR}/fb5-bot.dll"

exec dotnet "$BINARY_FILE" "$@"
